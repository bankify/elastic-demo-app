import React, { useState, useEffect } from "react";
import { View, StyleSheet, TextInput } from "react-native";
import font from "../config/font";
import color from "../config/color";
import Typography from "./Typography";
import spacing from "../config/spacing";

const FieldValue = props => {
  return (
    <View style={[styles.container, props.style]}>
      <Typography style={styles.title}>{props.title}</Typography>
      {props.inputProps ? (
        <TextInput
          style={[styles.content, props.inputProps.style]}
          {...props.inputProps}
        />
      ) : (
        <Typography style={styles.content}>{props.content}</Typography>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    minHeight: 40,
    borderBottomColor: color.lightGrey,
  },
  title: {
    fontSize: font.fontSizeTiny,
    fontFamily: font.fontFamilyMedium,
    color: color.darkColor,
    paddingBottom: spacing.s,
  },
  content: {
    fontFamily: font.fontFamilyMedium,
    fontSize: font.fontSizeNormal,
    color: color.darkColor,
    paddingBottom: spacing.xs,
  },
});

export default FieldValue;
