import React from "react";
import { View, StyleSheet, Text } from "react-native";
import color from "../config/color";
import font from "../config/font";

function ScreenHeader({ style, headerStyle, title, subtitle, children }) {
  return (
    <View style={[styles.container, style]}>
      {children}
      <Text style={[styles.title, headerStyle]}>{title}</Text>
      {subtitle && <Text style={styles.subtitle}>{subtitle}</Text>}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: color.whiteColor,
    padding: 40,
    paddingTop: 40,
    elevation: 1,
    shadowColor: color.darkColor,
    shadowOffset: { width: 0, height: -4 },
    shadowOpacity: 0.1,
    shadowRadius: 10,
  },

  title: {
    color: color.darkColor,
    fontSize: font.fontSizeExtraBig,
    fontFamily: font.fontFamilyBold,
    marginBottom: 10,
  },

  subtitle: {
    color: color.iconColor,
    fontSize: font.fontSizeNormal,
    fontFamily: font.fontFamily,
  },
});

export default ScreenHeader;
