import React from "react";
import { TouchableOpacity, Platform } from "react-native";
import { connectActionSheet } from "@expo/react-native-action-sheet";
import * as DocumentPicker from "expo-document-picker";
import * as ImagePicker from "expo-image-picker";

export const FilePickerButton = connectActionSheet(function _FilePickerButton({
  children,
  showActionSheetWithOptions,
  onPressShouldProceed,
  onFileResult,
  ...props
}) {
  const options = ["Camera Roll", "Choose Files", "Cancel"];

  const cancelButtonIndex = 2;
  return (
    <TouchableOpacity
      onPress={() => {
        const shouldProceed = onPressShouldProceed();
        if (shouldProceed) {
          if (Platform.OS === "web") {
            DocumentPicker.getDocumentAsync({
              type: "image/jpeg, application/pdf",
            }).then((result) => {
              if (result.type === "success") {
                onFileResult(result.uri);
              }
            });
          } else {
            showActionSheetWithOptions(
              { options, cancelButtonIndex },
              (buttonIndex) => {
                if (buttonIndex === 0) {
                  // Camera roll
                  ImagePicker.launchImageLibraryAsync({
                    mediaTypes: ImagePicker.MediaTypeOptions.Images,
                    quality: 1,
                  }).then((result) => {
                    if (!result.cancelled) {
                      onFileResult(result.uri);
                    }
                  });
                } else if (buttonIndex === 1) {
                  // File picker
                  DocumentPicker.getDocumentAsync({
                    type: "image/jpeg, application/pdf",
                  }).then((result) => {
                    if (result.type === "success") {
                      onFileResult(result.uri);
                    }
                  });
                }
              }
            );
          }
        }
      }}
      {...props}
    >
      {children}
    </TouchableOpacity>
  );
});
