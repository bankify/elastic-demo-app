import React from "react";
import {
  View,
  Image,
  TouchableWithoutFeedback,
  StyleSheet,
} from "react-native";
import BaseModal from "./BaseModel";
import color from "../config/color";
import PDFReader from "rn-pdf-reader-js";

function ImageModal({ open, onPressClose, uri, imageType }) {
  const Component = imageType === "pdf" ? PDFReader : Image;
  return (
    <BaseModal open={open} onPressClose={onPressClose}>
      <View
        style={{
          height: "85%",
          width: "95%",
          backgroundColor: color.whiteColor,
          borderRadius: 20,
          position: "relative",
          overflow: "hidden",
          alignItems: "center",
        }}
      >
        <TouchableWithoutFeedback onPress={onPressClose}>
          <View style={styles.modalOverlay} />
        </TouchableWithoutFeedback>
        <Component
          withPinchZoom
          style={{ height: "100%", width: "100%", borderRadius: 20 }}
          source={{
            uri,
          }}
        />
      </View>
    </BaseModal>
  );
}

const styles = StyleSheet.create({
  modalOverlay: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: "rgba(0,0,0,0.5)",
  },
});

export default ImageModal;
