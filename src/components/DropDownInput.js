import React from "react";
import {View, StyleSheet, Text, Platform, TextInput} from "react-native";
import RNPickerSelect from "react-native-picker-select";
import Buton from "./Button";
import color from "../config/color";
import font from "../config/font";
import {Ionicons} from "@expo/vector-icons";

function DropDownInput({
                           style,
                           data,
                           onSelect,
                           placeholder,
                           iconBackgroundColor,
                           title,
                           titleStyle,
                           icon,
                           headerAction,
                           dropDown = null,
                       }) {
    return (
        <View style={styles.shadow}>
            <View style={styles.header}>
                {!!title && (
                    <Text style={[titleStyle, {marginBottom: 10}]}>{title}</Text>
                )}
                {headerAction}
            </View>

            <View style={[styles.container, style]}>
                <View
                    style={[
                        styles.iconArea,
                        {
                            backgroundColor: iconBackgroundColor,
                            borderTopLeftRadius: style.borderRadius,
                            borderBottomLeftRadius: style.borderRadius,
                        },
                    ]}
                >
                    {icon}
                </View>
                <View style={{flex: 1}}>
                    {dropDown ? (
                        dropDown
                    ) : (
                        <RNPickerSelect
                            style={{
                                ...pickerSelectStyles,
                                iconContainer: {
                                    right: 20,
                                    top: Platform.OS === "ios" ? 0 : 15,
                                },
                            }}
                            onValueChange={onSelect}
                            items={data}
                            placeholder={placeholder}
                            Icon={() => {
                                return (
                                    <Ionicons
                                        name="ios-arrow-down"
                                        size={20}
                                        color={color.darkColor}
                                    />
                                );
                            }}
                        />
                    )}
                </View>
            </View>
        </View>
    );
}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: font.fontSizeSmall,
        color: color.darkColor,
        paddingLeft: 10,
        paddingRight: 30,
        fontFamily: font.fontFamily,
    },
    inputAndroid: {
        fontSize: font.fontSizeSmall,
        color: color.darkColor,
        paddingLeft: 10,
        paddingRight: 30,
        fontFamily: font.fontFamily,
    },
});

const styles = StyleSheet.create({
    shadow: {
        shadowColor: color.darkColor,
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 1,
    },
    header: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        marginBottom: 10,
    },
    container: {
        flex: 1,
        flexDirection: "row",
        backgroundColor: color.whiteColor,
        alignItems: "center",
        height: 40,
    },

    iconArea: {
        width: "30%",
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
    },
});

export default DropDownInput;
