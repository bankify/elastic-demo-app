import React from "react";
import { View, Text, StyleSheet } from "react-native";
import color from "../config/color";
import font from "../config/font";

const Typography = ({ style, children, textFont, size, textColor }) => {
  return (
    <Text
      style={[
        styles.defaultStyle,
        {
          color: textColor || color.darkColor,
          fontFamily: textFont || font.fontFamily,
          fontSize: size || font.fontSizeNormal
        },
        style
      ]}
    >
      {children}
    </Text>
  );
};

const styles = StyleSheet.create({
  defaultStyle: {
    textAlign: "left"
  }
});

export default Typography;
