import React, { useState, useEffect } from "react";
import { ScrollView, View, StyleSheet, Text, Platform } from "react-native";
import DropDownPicker from "./DropdownPicker";
import font from "../config/font";
import color from "../config/color";
import Typography from "./Typography";
import spacing from "../config/spacing";

const DropdownSelection = (props) => {
  return (
    <View
      style={[
        props.style,
        {
          ...(Platform.OS !== "android" && {
            zIndex: props.zIndex,
          }),
        },
      ]}
    >
      <Text style={styles.title}>{props.title}</Text>

      <DropDownPicker
        items={props.items}
        totalItems={props.items}
        placeholder={props.placeholder}
        defaultValue={props.selectedValue}
        dropDownMaxHeight={300}
        style={{
          backgroundColor: "transparent",
          borderBottomColor: color.lightGrey50,
          borderBottomWidth: 1,
        }}
        itemStyle={{
          justifyContent: "flex-start",
        }}
        placeholderStyle={{
          color: color.greyFont,
          fontFamily: font.fontFamilyMedium,
          fontSize: font.fontSizeNormal,
        }}
        labelStyle={styles.content}
        dropDownStyle={{
          backgroundColor: color.whiteColor,
          padding: spacing.m,
          color: color.categoryDropdown,
          fontFamily: font.fontFamilyMedium,
          fontSize: font.fontSizeNormal,
          ...styles.content,
        }}
        limit={props.limit || 6}
        searchablePlaceholder={"Search"}
        onChangeItem={props.onChangeItem}
        searchable={props.searchable}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: font.fontSizeTiny,
    fontFamily: font.fontFamilyMedium,
    color: color.darkColor,
    paddingBottom: spacing.s,
  },
  content: {
    fontFamily: font.fontFamilyMedium,
    color: color.darkColor,
    fontSize: font.fontSizeNormal,
    paddingBottom: spacing.xs,
  },
});

export default DropdownSelection;
