import React from "react";
import {
  TouchableOpacity,
  StyleSheet,
  Platform,
  TouchableNativeFeedback,
  ActivityIndicator,
  View,
} from "react-native";
import color from "../config/color";


const Touchable = Platform.select({
  android: TouchableNativeFeedback,
  default: TouchableOpacity,
});

function Button({ children, disabled, onPress, style, loading }) {
  return (
    <Touchable
      disabled={disabled}
      onPress={onPress}
      style={[
        styles.wrapper,
        disabled ? styles.disabled : styles.enabled,
        style || {},
      ]}
    >
      <View
        style={[
          styles.wrapper,
          Platform.OS === "ios" || Platform.OS === "web" ? {} : style,
        ]}
      >
        {loading ? (
          <ActivityIndicator size="small" color={color.whiteColor} />
        ) : (
          children
        )}
      </View>
    </Touchable>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    alignItems: "center",
    justifyContent: "center",
  },
  enabled: {},
  disabled: {},
});

export default Button;
