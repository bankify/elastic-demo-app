import React from "react";
import Modal from "react-native-modal";

function BaseModel({ children, open, onPressClose }) {
  return (
    <Modal
      animationType="slide"
      transparent={true}
      isVisible={open}
      style={{ justifyContent: "center", alignItems: "center" }}
      onBackButtonPress={onPressClose}
      onBackdropPress={onPressClose}
    >
      {children}
    </Modal>
  );
}

export default BaseModel;
