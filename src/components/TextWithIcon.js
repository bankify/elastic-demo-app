import React from "react";
import { View, Text } from "react-native";

function TextWithIcon({ icon, content, textStyle, iconAfter, style, onPress }) {
  return (
    <View
      style={{ flexDirection: "row", alignItems: "center", ...style }}
      onPress={onPress}
    >
      {icon}
      <Text style={[textStyle, { marginLeft: 5 }]}>{`${content}`}</Text>
      {iconAfter}
    </View>
  );
}

export default TextWithIcon;
