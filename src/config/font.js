export default {
  fontSizeExtraBig: 36,
  fontSizeMediumBig: 28,
  fontSizeBig: 24,
  fontSizeNormal: 18,
  fontSizeMedium: 16,
  fontSizeSmall: 14,
  fontSizeTiny: 12,
  fontSizeSuperTiny: 10,
};
