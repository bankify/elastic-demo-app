export default {
  xs: 5,
  s: 10,
  m: 20,
  l: 30,
  xl: 40,
};
