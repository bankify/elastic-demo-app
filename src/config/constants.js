if (typeof process.env.SCANNER_API_KEY !== 'string') {
    throw new Error('You need to define SCANNER_API_KEY variable, please see README.md file for more info');
}

export const scannerApiKey = process.env.SCANNER_API_KEY;