import React, { useReducer, useContext } from "react";
export const SET_RECEIPT = "SET RECEIPT";
//@ts-ignore
let DocumentContext = React.createContext();

let initialState = {
  date: "2018-07-21T00:00:00.000Z",
  time: null,
  ytunnus: null,
  paymentInfo: {},
  vats: [],
  items: [],
};
export const dateFormat = "dd.MM.yy";
let reducer = (state, action) => {
  console.log("SET RECEIPT ACTION", action);
  switch (action.type) {
    case SET_RECEIPT:
      return {
        ...state,
        ...action.payload,
        date: {
          value: action.payload.date?.value ?? new Date().toISOString(),
        },
      };
    default:
      return initialState;
  }
};

export function DocumentProvider({ children }) {
  let [state, dispatch] = useReducer(reducer, initialState);
  let value = { state, dispatch };

  return (
    <DocumentContext.Provider value={value}>
      {children}
    </DocumentContext.Provider>
  );
}

export function useDocument() {
  const { state, dispatch } = useContext(DocumentContext);
  return {
    receipt: state,
    dispatchDocument: dispatch,
  };
}
