import axios from "axios";

export async function scanReceipt({ imageBase64, documentType, apiKey }) {
  const body = {
    docType: documentType,
    content: imageBase64,
  };
  return axios.post(
    `https://scanner.apps.bankify.io/scan?key=${apiKey}&version=v2`,
    body
  );
}
