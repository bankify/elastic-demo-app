import React from "react";
import { createAppContainer } from "react-navigation";
import ExpenseNavigator from "./ExpenseNavigator";
import NavigationService from "./NavigationService";

const AppContainer = createAppContainer(ExpenseNavigator);

export default class App extends React.Component {
  render() {
    return (
      <AppContainer
        ref={(navigatorRef) => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}
