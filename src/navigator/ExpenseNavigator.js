import { createStackNavigator } from "react-navigation-stack";
import WelcomeScreen from "../screens/WelcomeScreen";
import CameraScreen from "../screens/Camera/CameraScreen";
import ExpenseInvoiceEditScreen from "../screens/ExpenseInvoiceEditScreen";
import ExpenseReceiptEditScreen from "../screens/ExpenseReceiptEditScreen";

export default createStackNavigator(
  {
    WelcomeScreen: {
      screen: WelcomeScreen,
    },
    CameraScreen: {
      screen: CameraScreen,
    },
    ExpenseInvoiceEditScreen: {
      screen: ExpenseInvoiceEditScreen,
    },
    ExpenseReceiptEditScreen: {
      screen: ExpenseReceiptEditScreen,
    },
  },
  {
    initialRouteName: "WelcomeScreen",
    defaultNavigationOptions: {
      headerShown: false,
    },
  }
);
