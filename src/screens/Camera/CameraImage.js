import React from 'react';
import {ActivityIndicator, StyleSheet, View} from "react-native";
import color from "../../config/color";
import Button from "../../components/Button";

const CameraImage = ({onCameraImage, isLoading}) => {
  function CaptureImageButton() {
    return (
      <View style={cameraImageStyles.container}>
        <View style={cameraImageStyles.secondLayer}>
          <View style={cameraImageStyles.firstLayer}/>
        </View>
      </View>
    );
  }

  return (
    <Button
      onPress={onCameraImage}
      disabled={isLoading}>
      {isLoading
        ? <ActivityIndicator size="small" color={color.whiteColor} />
        : <CaptureImageButton/>
      }
    </Button>
  )
};

export default CameraImage;

const cameraImageStyles = StyleSheet.create({
  container: {
    height: 50,
    width: 50,
    borderRadius: 25,
    backgroundColor: color.whiteColor,
    justifyContent: 'center',
    alignItems: 'center'
  },
  secondLayer: {
    height: 40,
    width: 40,
    borderRadius: 20,
    backgroundColor: color.cameraBackgroundColor,
    justifyContent: 'center',
    alignItems: 'center'
  },
  firstLayer: {
    height: 38,
    width: 38,
    borderRadius: 19,
    backgroundColor: color.whiteColor,
  }
});
