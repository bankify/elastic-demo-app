import React from 'react';
import {TouchableOpacity} from "react-native";
import {Camera} from "expo-camera";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import color from "../../config/color";

const RotationCamera = ({onRotationCamera}) => {
  return (
    <TouchableOpacity
      onPress={onRotationCamera}>
      <MaterialCommunityIcons name="rotate-3d" size={60} color={color.whiteColor}/>
    </TouchableOpacity>
  )
};

export default RotationCamera;
