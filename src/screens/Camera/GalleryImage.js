import React from 'react';
import {MaterialIcons} from "@expo/vector-icons";
import color from "../../config/color";
import Button from "../../components/Button";

const GalleryImage = ({isLoading, onGalleryImage}) => {
  return (
    <Button
      style={{
        position: 'absolute',
        bottom: 0,
        right: 0,
      }}
      disabled={isLoading}
      onPress={() => onGalleryImage()}>
      <MaterialIcons name={"photo"} size={40} color={color.whiteColor}/>
    </Button>
  )
};

export default GalleryImage;
