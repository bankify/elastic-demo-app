import React, {useState, useEffect, useRef} from "react";
import {View, StyleSheet} from "react-native";
import {useSafeAreaInsets} from "react-native-safe-area-context";
import {connectActionSheet} from "@expo/react-native-action-sheet";
import {Camera} from "expo-camera";
import * as FileSystem from "expo-file-system";
import * as DocumentPicker from "expo-document-picker";
import GalleryImage from "./GalleryImage";
import CameraImage from "./CameraImage";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import * as ImageManipulator from "expo-image-manipulator";
import {scanReceipt} from "../../services/ScannerServices";
import {MaterialIcons} from "@expo/vector-icons";
import color from "../../config/color";
import {useScanner, ADD_IMAGE} from "../../context/ScannerContext";
import {useDocument, SET_RECEIPT} from "../../context/DocumentContext";
import {scannerApiKey} from "../../config/constants";

export default connectActionSheet(function CameraScreen({
                                                            navigation,
                                                            showActionSheetWithOptions,
                                                        }) {
    const [hasPermission, setHasPermission] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const cameraRef = useRef(null);
    const {dispatchScanner} = useScanner();
    const {dispatchDocument} = useDocument();
    const documentType = navigation.getParam("documentType");

    const {top, bottom} = useSafeAreaInsets();

    useEffect(() => {
        (async () => {
            try {
                const {status} = await Camera.requestPermissionsAsync();
                setHasPermission(status === "granted");
            } catch (e) {
                console.log("error in getting permission", e);
            }
        })();
    }, []);

    const onCameraImage = async () => {
        if (cameraRef.current) {
            const options = {quality: 1};
            const image = await cameraRef.current.takePictureAsync(options);
            await handleScanImage(image);
        }
    };

    const onGalleryImage = () => {
        // Same interface as https://facebook.github.io/react-native/docs/actionsheetios.html
        const options = ["Photo Gallery", "Browse Files", "Cancel"];

        const cancelButtonIndex = 2;

        showActionSheetWithOptions(
            {
                options,
                cancelButtonIndex,
            },
            async (buttonIndex) => {
                if (buttonIndex === 0) {
                    const {status} = await Permissions.askAsync(
                        Permissions.CAMERA_ROLL
                    );
                    if (status !== "granted") {
                        return alert("No camera Permission!");
                    }
                    let result = await ImagePicker.launchImageLibraryAsync({
                        mediaTypes: ImagePicker.MediaTypeOptions.All,
                        quality: 1,
                    });

                    if (!result.cancelled) {
                        await handleScanImage(result);
                    }
                } else if (buttonIndex === 1) {
                    DocumentPicker.getDocumentAsync({
                        type: "application/pdf",
                    }).then(async (result) => {
                        try {
                            if (result.uri.includes(".pdf")) {
                                setIsLoading(true);
                                const base64 = await FileSystem.readAsStringAsync(result.uri, {
                                    encoding: FileSystem.EncodingType.Base64,
                                });
                                let res = await scanReceipt({
                                    imageBase64: `data:application/pdf;base64,${base64}`,
                                    documentType: documentType.toUpperCase(),
                                    apiKey: scannerApiKey,
                                });
                                dispatchDocument({type: SET_RECEIPT, payload: res.data});
                                navigation.navigate(`Expense${documentType}EditScreen`);
                            } else {
                                await handleScanImage(result);
                            }
                        } finally {
                            setIsLoading(false);
                        }
                    });
                }
            }
        );
    };

    async function handleScanImage(image) {
        try {
            setIsLoading(true);
            const newDimensionResult = resizeImage(image.width, image.height);
            const resizeResult = await ImageManipulator.manipulateAsync(
                image.uri,
                [
                    {
                        resize: {
                            width: newDimensionResult.width,
                            height: newDimensionResult.height,
                        },
                    },
                ],
                {base64: true}
            );

            dispatchScanner({type: ADD_IMAGE, payload: resizeResult});
            let res = await scanReceipt({
                imageBase64: `data:image/jpeg;base64,${resizeResult.base64}`,
                documentType: documentType.toUpperCase(),
                apiKey: scannerApiKey,
            });
            dispatchDocument({type: SET_RECEIPT, payload: res.data});
            navigation.navigate(`Expense${documentType}EditScreen`);
        } catch (error) {
            console.log(error, "something wrong in uploading receipt");
        } finally {
            setIsLoading(false);
        }
    }

    return (
        <View style={styles.flex1}>
            <Camera
                style={styles.flex1}
                type={Camera.Constants.Type.back}
                ref={cameraRef}
            />
            <View style={StyleSheet.absoluteFill}>
                <View style={{flex: 1, paddingTop: top, paddingBottom: bottom}}>
                    <View style={styles.header}>
                        <MaterialIcons
                            onPress={() => navigation.goBack()}
                            style={styles.backBtn}
                            name="cancel"
                            size={40}
                            color={color.whiteColor}
                        />
                    </View>
                    <View style={styles.container}>
                        <View style={styles.imageWrapper}>
                            <CameraImage
                                isLoading={isLoading}
                                onCameraImage={onCameraImage}
                            />
                            <GalleryImage
                                isLoading={isLoading}
                                onGalleryImage={onGalleryImage}
                            />
                        </View>
                    </View>
                </View>
            </View>
        </View>
    );
});

const resizeImage = (width, height) => {
    let aspectRatio = width / height;
    if (width < 1200 && height < 1200 && width < height) {
        return {width, height};
    } else {
        if (aspectRatio > 1) {
            let newHeight = width;
            let newWidth = height;
            return resizeImage(newWidth, newHeight);
        } else {
            height = height - 50;
            width = Math.floor(height * aspectRatio);
            return resizeImage(width, height);
        }
    }
};

const styles = StyleSheet.create({
    flex1: {
        flex: 1,
    },
    container: {
        flex: 1,
        backgroundColor: "transparent",
        flexDirection: "row",
        padding: 20,
    },
    header: {
        flexDirection: "row",
        justifyContent: "space-between",
        padding: 20,
    },
    imageWrapper: {
        flex: 1,
        alignSelf: "flex-end",
        flexDirection: "row",
        justifyContent: "center",
        marginBottom: 30,
    },
});
