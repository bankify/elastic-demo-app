import React, { useState } from "react";
import { View, Text, StyleSheet, Platform } from "react-native";

import ScreenHeader from "../../components/ScreenHeader";
import color from "../../config/color";
import FieldValue from "../../components/FieldValue";
import Button from "../../components/Button";
import { Ionicons, SimpleLineIcons } from "@expo/vector-icons";
import font from "../../config/font";
import TextWithIcon from "../../components/TextWithIcon";

import { useDocument } from "../../context/DocumentContext";
import ImageModal from "../../components/ImageModal";

import { useScanner } from "../../context/ScannerContext";
import spacing from "../../config/spacing";

export const Header = ({ navigation }) => {
	return (
		<ScreenHeader
			headerStyle={{ fontSize: font.fontSizeMediumBig }}
			title={"Document Details"}
			subtitle={null}
		>
			<View style={styles.row}>
				<Button
					onPress={() => {
						navigation.popToTop();
					}}
				>
					<Ionicons name="ios-arrow-back" size={30} color={color.blue} />
				</Button>
			</View>
		</ScreenHeader>
	);
};

const convertListData = (array, fieldName) => {
	return [...array].map((item) => ({ label: item[fieldName], value: item }));
};

export const ViewDocument = () => {
	const { scanner } = useScanner();
	const [openImageModal, setOpenImageModal] = useState(false);

	if (Platform.OS === "web") {
		return null;
	}

	return (
		<>
			<View style={[styles.row, { marginVertical: spacing.m }]}>
				<Button onPress={() => setOpenImageModal(true)}>
					<TextWithIcon
						icon={<SimpleLineIcons name="eye" size={20} color={color.blue} />}
						content={"View document"}
						textStyle={{
							color: color.blue,
							fontFamily: font.fontFamily,
							fontSize: font.fontSizeSmall,
						}}
					/>
				</Button>
			</View>
			<ImageModal
				open={openImageModal}
				onPressClose={() => {
					setOpenImageModal(false);
				}}
				uri={scanner.imageBase64.uri}
			/>
		</>
	);
};

export const Total = ({ amount, currency }) => {
	// added currency
	return (
		<View
			style={{
				flexDirection: "row",
				marginTop: spacing.m,
				alignItems: "center",
				justifyContent: "space-between",
			}}
		>
			<Text
				style={{
					color: color.darkColor,
					fontFamily: font.fontFamilyMedium,
					fontSize: font.fontSizeNormal,
				}}
			>
				{"Total Amount"}
			</Text>
			<View
				style={{
					justifyContent: "center",
					alignItems: "center",
					borderWidth: 1,
					borderColor: color.blue25,
					borderRadius: 10,
					padding: 10,
					flexDirection: "row",
				}}
			>
				<Text
					style={{
						color: color.blue25,
						fontFamily: font.fontFamily,
						fontSize: font.fontSizeSmall,
					}}
				>
					{amount} {currency}
				</Text>
			</View>
		</View>
	);
};

export const ReceiptItems = () => {
	const { receipt } = useDocument();
	return (
		<>
			{receipt.items?.map((item, index) => (
				<View key={index} style={[styles.twoColumn, styles.columnSpace]}>
					<FieldValue
						title={"TITLE"}
						style={{ width: "45%" }}
						content={item.title}
					/>

					<FieldValue
						title={"AMOUNT"}
						content={item.price}
						style={{ width: "45%" }}
					/>
				</View>
			))}
		</>
	);
};

export const VATField = () => {
	const { receipt } = useDocument();

	return (
		<>
			{receipt.vats?.map((vat, index) => (
				<View
					key={index}
					style={[
						styles.twoColumn,
						styles.columnSpace,
						{
							...(Platform.OS !== "android" && {
								zIndex: 6000 - index,
							}),
						},
					]}
				>
					<FieldValue
						title={"VAT"}
						style={{ width: "45%" }}
						content={vat.amount?.toString()}
					/>

					<FieldValue
						title={"PERCENT"}
						content={vat.percent}
						style={{ width: "45%" }}
					/>
				</View>
			))}
		</>
	);
};

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		width: "100%",
		paddingBottom: 80,
	},
	contentWrapper: {
		marginTop: spacing.m,
		paddingHorizontal: spacing.m,
	},
	content: {
		padding: 8,
	},
	supplierButton: {
		marginTop: spacing.m,
		alignItems: "flex-end",
	},
	input: {
		color: color.darkColor,
		fontSize: font.fontSizeNormal,
		width: "45%",
		height: 40,
		marginBottom: spacing.m,
		borderBottomWidth: 1,
		borderBottomColor: color.lightGrey50,
	},
	row: {
		width: "100%",
		flexDirection: "row",
		justifyContent: "space-between",
		marginBottom: spacing.m,
	},
	comment: {
		fontFamily: font.fontFamily,
		paddingTop: 10,
		padding: 10,
		height: 150,
		width: "100%",
		backgroundColor: color.whiteColor,
		marginTop: 10,
		marginBottom: spacing.m,
		borderRadius: 10,
		shadowColor: color.darkColor,
		shadowOffset: { width: 0, height: 5 },
		shadowOpacity: 0.1,
		shadowRadius: 8,
		elevation: 1,
		textAlignVertical: "top",
	},
	columnSpace: {
		marginBottom: spacing.s,
	},

	twoColumn: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "flex-end",
	},
	rightButton: {
		marginTop: spacing.m,
		alignItems: "flex-end",
	},
});
