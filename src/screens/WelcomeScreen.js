import React, { useState } from "react";
import { View, StyleSheet, Alert, Platform, Text } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import * as DocumentPicker from "expo-document-picker";
import { scanReceipt } from "../services/ScannerServices";
import color from "../config/color";
import font from "../config/font";
import { useDocument, SET_RECEIPT } from "../context/DocumentContext";
import { useScanner, ADD_IMAGE } from "../context/ScannerContext";
import ScreenHeader from "../components/ScreenHeader";
import Button from "../components/Button";
import { FontAwesome5 } from "@expo/vector-icons";
import TextWithIcon from "../components/TextWithIcon";
import spacing from "../config/spacing";
import {scannerApiKey} from "../config/constants";

export const INVOICE = "Invoice";
export const RECEIPT = "Receipt";

function WelcomeScreen({ navigation }) {
  const { top } = useSafeAreaInsets();
  const { dispatchDocument } = useDocument();
  const { dispatchScanner } = useScanner();

  const [isLoading, setIsLoading] = useState(false);

  async function handleScanImage(image, documentType) {
    try {
      setIsLoading(true);

      dispatchScanner({ type: ADD_IMAGE, payload: image.uri });
      let res = await scanReceipt({
        imageBase64: image.uri,
        documentType: documentType.toUpperCase(),
        apiKey: scannerApiKey,
      });
      dispatchDocument({ type: SET_RECEIPT, payload: res.data });
      navigation.navigate(`Expense${documentType}EditScreen`);
    } catch (error) {
      console.log(error, "something wrong in uploading receipt");
    } finally {
      setIsLoading(false);
    }
  }

  async function handleScanPdf(pdf, documentType) {
    setIsLoading(true);
    try {
      let res = await scanReceipt({
        imageBase64: pdf.uri,
        documentType: documentType.toUpperCase(),
        apiKey: scannerApiKey,
      });
      dispatchDocument({ type: SET_RECEIPT, payload: res.data });
      navigation.navigate(`Expense${documentType}EditScreen`);
    } catch (error) {
      console.log(error);
      alert("Failed to scan..");
    } finally {
      setIsLoading(false);
    }
  }

  return (
    <View style={styles.container}>
      <ScreenHeader
        style={{ paddingTop: top + spacing.s, paddingHorizontal: 20 }}
        title={"Scan"}
        subtitle={"You can scan a file or open the camera"}
      ></ScreenHeader>
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        {isLoading && (
          <Text style={{ marginVertical: 20 }}>Scanning, please wait...</Text>
        )}
        <Button
          disabled={isLoading}
          onPress={() => {
            if (Platform.OS === "web") {
              DocumentPicker.getDocumentAsync({
                type: "application/pdf, image/jpeg",
              }).then(async (result) => {
                if (result.uri.includes("data:application/pdf;base64")) {
                  handleScanPdf(result, RECEIPT);
                } else {
                  handleScanImage(result, RECEIPT);
                }
              });
            } else {
              navigation.navigate("CameraScreen", {
                documentType: RECEIPT,
              });
            }
          }}
          style={styles.button}
        >
          <TextWithIcon
            icon={
              <FontAwesome5 name="receipt" size={20} color={color.whiteColor} />
            }
            content={"Scan Receipt"}
            textStyle={{
              color: color.whiteColor,
              fontFamily: font.fontFamily,
              fontSize: font.fontSizeMedium,
            }}
          />
        </Button>
        <Button
          disabled={isLoading}
          onPress={() => {
            if (Platform.OS === "web") {
              DocumentPicker.getDocumentAsync({
                type: "application/pdf, image/jpeg",
              }).then(async (result) => {
                if (result.uri.includes("data:application/pdf;base64")) {
                  handleScanPdf(result, INVOICE);
                } else {
                  handleScanImage(result, INVOICE);
                }
              });
            } else {
              navigation.navigate("CameraScreen", {
                documentType: INVOICE,
              });
            }
          }}
          style={styles.button}
        >
          <TextWithIcon
            icon={
              <FontAwesome5
                name="file-invoice"
                size={20}
                color={color.whiteColor}
              />
            }
            content={"Scan Invoice"}
            textStyle={{
              color: color.whiteColor,
              fontFamily: font.fontFamily,
              fontSize: font.fontSizeMedium,
            }}
          />
        </Button>
      </View>
    </View>
  );
}

const resizeImage = (width, height) => {
  let aspectRatio = width / height;
  if (width < 1200 && height < 1200 && width < height) {
    return { width, height };
  } else {
    if (aspectRatio > 1) {
      let newHeight = width;
      let newWidth = height;
      return resizeImage(newWidth, newHeight);
    } else {
      height = height - 50;
      width = Math.floor(height * aspectRatio);
      return resizeImage(width, height);
    }
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: color.blueBackground,
  },

  header: {
    width: "100%",
  },

  contentContainer: {
    flex: 1,
    paddingTop: 20,
    alignItems: "center",
    width: "100%",
    paddingHorizontal: 20,
  },
  linkTo: {
    color: color.blue25,
  },
  headerWrapper: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    marginBottom: 10,
  },
  button: {
    borderRadius: 25,
    backgroundColor: color.blue25,
    paddingHorizontal: 14,
    paddingVertical: 8,
    marginBottom: 10,
  },
});

export default WelcomeScreen;
