import React from "react";
import { StyleSheet, View, Platform } from "react-native";
import DropdownSelection from "../components/DropdownSelection";
import FieldValue from "../components/FieldValue";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import { useDocument, SET_RECEIPT } from "../context/DocumentContext";
import get from "lodash/get";
import {
	VATField,
	Total,
	ViewDocument,
	Header,
	styles as commonStyles,
} from "./InvoiceReceipt/SubComponents";
import spacing from "../config/spacing";
import { format } from "date-fns";

function ExpenseInvoiceEditScreen({ navigation }) {
	const { receipt, dispatchDocument } = useDocument();

	return (
		<KeyboardAwareScrollView style={styles.container}>
			<Header navigation={navigation} />

			<View style={styles.contentWrapper}>
				<View
					style={{
						...(Platform.OS !== "android" && {
							zIndex: 101,
						}),
					}}
				>
					<DropdownSelection
						title={"Document Type"}
						selectedValue={"Invoice"}
						items={[
							{ label: "INVOICE", value: "Invoice" },
							{ label: "RECEIPT", value: "Receipt" },
						]}
						onChangeItem={(item) => {
							navigation.replace(`Expense${item.value}EditScreen`);
						}}
						placeholder={"Document Type"}
					/>
				</View>

				<ViewDocument />

				<View
					style={[
						commonStyles.twoColumn,
						commonStyles.columnSpace,
						{
							...(Platform.OS !== "android" && {
								zIndex: 99,
							}),
						},
					]}
				>
					<FieldValue
						title={"Date"}
						content={
							receipt.paymentInfo?.date
							/* ? format(new Date(receipt.paymentInfo?.date), "dd/MM/yyyy")
								: "N/A" */ // commented since dates likes "28.10.2019" cause an error
						}
						style={{ width: "45%" }}
					/>
					<FieldValue
						title={"Due Date"}
						style={{ width: "45%" }}
						content={
							receipt.paymentInfo?.dueDate
							/* ? format(new Date(receipt.paymentInfo?.dueDate), "dd/MM/yyyy")
								: "N/A" */ // commented since dates likes "28.10.2019" cause an error
						}
					/>
				</View>

				{!!receipt.paymentInfo?.reference && ( // conditional rendering of individual fields, double negation to convert falsy into Boolean false
					<FieldValue
						style={commonStyles.columnSpace}
						title={"Reference Number"}
						content={receipt.paymentInfo?.reference} // removed  '?? "" ' as the check if field is falsy already happens above
					/>
				)}

				{!!receipt.paymentInfo?.iban && (
					<FieldValue
						style={commonStyles.columnSpace}
						title={"IBAN"}
						content={receipt.paymentInfo?.iban}
						inputProps={{
							value: receipt.paymentInfo?.iban,
							onChangeText: (text) =>
								dispatchDocument({
									type: SET_RECEIPT,
									payload: {
										...receipt,
										paymentInfo: {
											...(receipt.paymentInfo ?? {}),
											iban: text,
										},
									},
								}),
						}}
					/>
				)}

				{!!receipt.paymentInfo?.supplier && (
					<FieldValue
						style={commonStyles.columnSpace}
						title={"Supplier"}
						content={receipt.paymentInfo?.supplier}
					/>
				)}

				{!!receipt.paymentInfo?.wbsCode && (
					<FieldValue
						style={commonStyles.columnSpace}
						title={"WBS Code"}
						content={receipt.paymentInfo?.wbsCode}
					/>
				)}

				{!!receipt.paymentInfo?.subscriberId && (
					<FieldValue
						style={commonStyles.columnSpace}
						title={"Subscriber ID"}
						content={receipt.paymentInfo?.subscriberId}
					/>
				)}

				{!!receipt.paymentInfo?.subscriberName && (
					<FieldValue
						style={commonStyles.columnSpace}
						title={"Subscriber Name"}
						content={receipt.paymentInfo?.subscriberName}
					/>
				)}

				{!!receipt.paymentInfo?.amountWithoutTax && (
					<FieldValue
						style={commonStyles.columnSpace}
						title={"Amount Without Tax"}
						content={receipt.paymentInfo?.amountWithoutTax}
					/>
				)}

				{!!receipt.paymentInfo?.vat && (
					<FieldValue
						style={commonStyles.columnSpace}
						title={"VAT"}
						content={receipt.paymentInfo?.vat}
					/>
				)}

				<Total
					amount={get(receipt, "paymentInfo.amount")}
					currency={get(receipt, "paymentInfo.currency")}
					onChangeAmount={(rawAmount) => {
						dispatchDocument({
							type: SET_RECEIPT,
							payload: {
								...receipt,
								paymentInfo: {
									...(receipt.paymentInfo ?? {}),
									amount: rawAmount,
								},
							},
						});
					}}
				/>
			</View>
		</KeyboardAwareScrollView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		width: "100%",
		paddingBottom: spacing.xl,
	},
	contentWrapper: {
		marginTop: spacing.m,
		paddingHorizontal: spacing.l,
	},
});

export default ExpenseInvoiceEditScreen;
