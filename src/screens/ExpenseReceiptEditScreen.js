import React from "react";
import { View, StyleSheet, Platform } from "react-native";
import DropdownSelection from "../components/DropdownSelection";
import color from "../config/color";
import FieldValue from "../components/FieldValue";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import font from "../config/font";
import { useDocument } from "../context/DocumentContext";

import {
  Total,
  ViewDocument,
  VATField,
  Header,
  styles as commonStyles,
  ReceiptItems,
} from "./InvoiceReceipt/SubComponents";
import spacing from "../config/spacing";

function ExpenseReceiptEditScreen({ navigation }) {
  const { receipt } = useDocument();

  return (
    <KeyboardAwareScrollView style={styles.container}>
      <Header navigation={navigation} />

      <View style={styles.contentWrapper}>
        <View
          style={{
            ...(Platform.OS !== "android" && {
              zIndex: 101,
            }),
          }}
        >
          <DropdownSelection
            title={"Document Type"}
            selectedValue={"Receipt"}
            items={[
              { label: "INVOICE", value: "Invoice" },
              { label: "RECEIPT", value: "Receipt" },
            ]}
            onChangeItem={(item) => {
              navigation.replace(`Expense${item.value}EditScreen`);
            }}
            placeholder={"Document Type"}
          />
        </View>

        <ViewDocument />

        <View
          style={[
            commonStyles.twoColumn,
            commonStyles.columnSpace,
            {
              ...(Platform.OS !== "android" && {
                zIndex: 100,
              }),
            },
          ]}
        >
          <FieldValue
            title={"Date"}
            content={receipt.date?.value ?? "N/A"}
            style={{ width: "45%" }}
          />
        </View>

        <ReceiptItems />

        <VATField />

        <Total amount={receipt.paymentInfo?.amount ?? 0} />
      </View>
    </KeyboardAwareScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    paddingBottom: spacing.xl,
  },
  contentWrapper: {
    marginTop: 20,
    paddingHorizontal: 20,
  },
  content: {
    padding: 8,
  },
  rightButton: {
    marginTop: 15,
    alignItems: "flex-end",
  },
  input: {
    color: color.darkColor,
    fontSize: font.fontSizeNormal,
    width: "45%",
    height: 40,
    marginBottom: 20,
    borderBottomWidth: 1,
    borderBottomColor: color.lightGrey50,
  },
  row: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  comment: {
    fontFamily: font.fontFamily,
    paddingTop: 10,
    padding: 10,
    height: 150,
    width: "100%",
    backgroundColor: color.whiteColor,
    marginTop: 10,
    marginBottom: 20,
    borderRadius: 10,
    shadowColor: color.darkColor,
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.1,
    shadowRadius: 8,
    elevation: 1,
    textAlignVertical: "top",
  },
  columnSpace: {
    marginVertical: 10,
  },
  twoColumn: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
});

export default ExpenseReceiptEditScreen;
