import React from "react";
import { ActionSheetProvider } from "@expo/react-native-action-sheet";
import { SafeAreaProvider } from "react-native-safe-area-context";
import RootNavigator from "./src/navigator/RootNavigator";
import { ScannerProvider } from "./src/context/ScannerContext";
import { DocumentProvider } from "./src/context/DocumentContext";

export default function App() {
  return (
    <SafeAreaProvider>
      <ActionSheetProvider>
        <DocumentProvider>
          <ScannerProvider>
            <RootNavigator />
          </ScannerProvider>
        </DocumentProvider>
      </ActionSheetProvider>
    </SafeAreaProvider>
  );
}
