### Elastic Demo Application

#### Requirements

* You need an API Key first, then create a file named `.env` with the following content:

```
SCANNER_API_KEY=<YOUR API KEY>
```

#### Run the project

* Clone
* `yarn install`
* Run ios: `yarn ios`
* Run android: `yarn android`
* Run web: `yarn web`
